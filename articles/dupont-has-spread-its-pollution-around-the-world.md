---
published: 2019-10-27T16:14:57.000+00:00
title: DuPont Has Spread Its Pollution Around the World
description: 'DuPont opened a factory in Saudi Arabia last week that will produce
  reverse osmosis water filters. The filters use ultra-thin membranes to remove water
  impurities, including PFAS — chemicals made and used by DuPont that have caused
  widespread water contamination around the world.

'
image: "/uploads/cover_dupont-water-filter-saudi-arabia.jpg"
alt: Sadara Chemistery view through an aircraft window
descripion: Sadara Chemical Company, a joint venture between Saudi Aramco and Dow
  Chemical, in Jubail, Saudi Arabia, on Jan. 11, 2018. The new production plant will
  make reverse osmosis technology to be used by the Sadara complex.
en_vedette: true
cover:
  image: ''
  alt: ''
  legend: ''

---
\# DuPont Has Spread Its Pollution Around the World. Now It Wants to Filter Your Contaminated Drinking Water.

  
DuPont opened a factory in Saudi Arabia last week that will produce reverse osmosis water filters. The filters use ultra-thin membranes to remove water impurities, including PFAS — chemicals made and used by DuPont that have caused widespread water contamination around the world.  
Reverse osmosis is one of the technologies that the Environmental Protection Agency recommends for reducing water contamination from PFAS chemicals, which are associated with cancers, immune dysfunction, reproductive issues, and other health problems. According to the agency’s website, reverse osmosis “membranes are typically more than 90 percent effective at removing a wide range of PFAS, including shorter chain PFAS.”  
DuPont Water Solutions, a division of DowDuPont that focuses on water filtration, opened the plant with a ribbon-cutting ceremony in Jubail, Saudi Arabia, on December 3. “Milestone achievement improves direct access to potable and industrial water solutions,” announced a DuPont press release about the plant, which is expected to begin production early next year.Join Our NewsletterOriginal reporting. Fearless journalism. Delivered to you.I’m in  
The reverse osmosis business was previously owned by Dow, which has had a production facility in Minnesota since 1977 and merged with DuPont last year. The new production plant will make reverse osmosis technology to be used by the Sadara Chemical Company complex, a joint venture developed by Dow and Saudi Aramco, the country’s oil and gas company. The plant’s filters will also be used in Saudi Arabia and other parts of the Middle East, as well as Africa, Eastern Europe, India, China, and Southeast Asia. In addition to removing industrial pollutants such as PFAS, the membranes can desalinate seawater.  
“This new production line is providing game-changing innovation to help municipalities, businesses and people thrive by enabling sustainable access to clean, high-quality water,” Marc Doyle, chief operating officer for the Specialty Products Division at DowDuPont who attended the event, said in a press release.  
An executive from DuPont Water Solutions who was also at the ribbon-cutting, H.P. Nanda, emphasized the company’s role in cleaning up water contamination. “We remain committed to delivering solutions that help purify and reclaim water sources, especially in areas facing water scarcity and resource challenges.”  
But Al Telsey, an attorney suing DuPont over massive contamination from PFOA and more than 1,000 other chemicals in New Jersey, is one of the many people contending with PFAS contamination from DuPont who may be more focused on the company’s role putting the chemicals into water than removing them.  
“These guys are polluters, not water cleaners,” said Telsey, who nevertheless acknowledged the business acumen involved in getting into the filtration business.  
“DuPont has learned the art of making money coming and going,” said Telsey. “They profited off the environmental contamination and now can profit on cleaning it up. It’s quite a feat.”