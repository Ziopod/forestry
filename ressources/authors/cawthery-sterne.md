---
guid: 74ab0524-f9c5-4f68-a33c-95cec88f587c
name: Cawthery Sterne
twitter: scawthery1
avatar: https://robohash.org/temporearchitectooptio.bmp?size=50x50&set=set1
web: http://addtoany.com/sit/amet.jpg
email: scawthery1@furl.net
address:
  street: 80380 Esch Center
  postal: 814 41
  city: Skutskär
  country: Sweden
company:
  name: Towne Inc
  slogan: utilize efficient markets
job: Safety Technician IV
quote: Multi-lateral uniform open system
---