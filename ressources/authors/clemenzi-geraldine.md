---
guid: 3e77957e-d7d9-452a-bd41-d91b3f743df6
name: Clemenzi Géraldine
twitter: fclemenzi1
avatar: https://robohash.org/aspernaturearumminima.bmp?size=50x50&set=set1
web: http://hc360.com/posuere/nonummy/integer/non/velit/donec/diam.jpg
email: tclemenzi1@reference.com
quote: Fully-configurable client-server groupware
address:
  street: 5 Carberry Road
  postal: 99101
  city: Kittilä
  country: Finland
company:
  name: Powlowski LLC
  slogan: maximize enterprise paradigms
job: Speech Pathologist
---
