---
title: À propos de Ghost
navigation:
- membre: Membre
  a-propos: À propos
- contacts: Contacts
- editeur: L'éditeur
- a-propos-de-ghost: À propos
- devenir-membre: Membre
- nous-contacter: Contacts
- a-propos-de-ghost-media: L'éditeur

---
## À propos de Ghost

*[Description de storytelling - 1000 signes, 3 paragraphes]*

After NSA whistleblower Edward Snowden came forward with revelations of mass surveillance in 2013, journalists Glenn Greenwald, Laura Poitras, and Jeremy Scahill decided to found a new media organization dedicated to the kind of reporting those disclosures required: fearless, adversarial journalism. They called it The Intercept.

Today, The Intercept is an award-winning news organization that covers national security, politics, civil liberties, the environment, international affairs, technology, criminal justice, the media, and more. The Intercept gives its journalists the editorial freedom and legal support they need to pursue investigations that expose corruption and injustice wherever they find it and hold the powerful accountable.

EBay founder and philanthropist Pierre Omidyar provided the funding to launch The Intercept and continues to support it through First Look Media Works, a nonprofit organization.

## Devenir membre

*[Engagment à participer - 300 signes]*

The kind of journalism we do is costly, and we need readers’ support to help keep The Intercept strong and independent. That’s why we have established a sustaining membership program that makes it easy to invest in courageous, quality reporting you can trust. Please click here to learn more about our membership program.

## Nous contacter

*[Engagement à prendre contact + contacte général - 150 signes]*

<!-- Afficher en multi-colonnes -->
### Presse

*[Identifcation de l'interlocuteur + contact spécialisé - §0 signes]*

Are you a member of the press? Contact our [media team](mailto:media@ghost.org).

### Membres 
If you have a membership question, contact our [membership team](mailto:membership@ghost.org).

### Travailler avec nous
Interested in joining the Intercept team? View our [job listings](jobs).

### Participer
To submit a proposal for consideration, please email it to <submissions@theintercept.com>.

## L'équipe

*[Descrition des acteurs - visuel + description 100 signes + contact]*

 - ![Portrait de Ryan Tate](images/portraits/ryan-tate.jjpg)
   #### Ryan Tate
   Technology Editor

   Ryan Tate has written about the use, abuse, and subversion of corporate power in the technology sector and beyond. 

   <ryan.tate@theintercept.com>

 - ![Portrait de John Thomason](images/portraits/john-thomason.jpg)
   ####John Thomason
   Research Editor for Fact Checking

   John Thomason is a research editor based in New York City. 

   <john.thomason@theintercept.com>

 - ![Portrait de MArgot Williams](images/portraits/margot-williams.jpg)
   #### Margot Williams
   Research Editor for Investigations

   Margot Williams is a journalist and research editor who was part of two Pulitzer Prize-winning teams at the Washington Post. 

   <margot.williams@theintercept.com>

## À propos de GhostMedia

*[description de l'organisme éditeur - 500 signes]*

The Intercept is a publication of First Look Media. Launched in 2013 by eBay founder and philanthropist Pierre Omidyar, First Look Media is a multi-platform media company devoted to supporting independent voices, from fearless investigative journalism and documentary filmmaking to arts, culture, media and entertainment. First Look Media produces and distributes content in a wide range of forms including feature films, short-form video, podcasts, interactive media and long-form journalism, for its own digital properties and with partners.