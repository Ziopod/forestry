---
cover:
  image: "/uploads/cover_dupont-water-filter-saudi-arabia.jpg"
  alt: Hublot
  legend: Décollage
edito: Texte de présentation.
related:
- articles/hello.md
- articles/dupont-has-spread-its-pollution-around-the-world.md
sections:
- template: section
  link:
    color: "#4A90E2"
    name: Call to action
    url: http://example.com
  content: Section A

---
